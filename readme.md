# REFACTORING

## INTRO

L'idée ici est d'effectuer un refactoring de la fonction js editor_UpdateElements legacy.

on compare ensuite les tests créés par le refactoring appliqué sur le code legacy.

## PROBLEMS

Après la modification, il reste 2 tests qui ne passent pas sur le code legacy.  
Cependant les valeurs retournées par le legacy sont invalides.  
On est donc allé plus(trop?) loin que le refactoring en corrigeant(changeant) le comportement.

## HOW TO

run tests

```bash
$ npm install
$ npm test
```

## CODE

le code legacy en question

```javascript
function editor_UpdateElements(contentElement, tagName, attrName) {
  var elements = this._getElements(contentElement, tagName);

  if (elements) {
    for (var i = 0; i < elements.length; i++) {
      var attrValue = elements[i].getAttribute(attrName, 2);

      var mergeFieldFound = extractMergedFieldFromUrl(attrName, attrValue);
      if (mergeFieldFound !== null) {
        elements[i].setAttribute("href", mergeFieldFound);
      } else {
        var arr = window.location.href.split("/");
        var path = arr[0] + "//" + arr[2];
        if (
          "href" === attrName &&
          attrValue &&
          attrValue.search("[%(*)%]") == -1 &&
          attrValue.search("http://") == -1 &&
          attrValue.search("https://") == -1 &&
          attrValue.search("mailto:") == -1
        ) {
          elements[i].setAttribute("href", '"' + path + attrValue + '"');
        } else if ("src" == attrName && attrValue) {
          elements[i].setAttribute(
            "src",
            '"' + path + attrValue.replace(/\"/gi, "%22") + '"'
          );
        }
      }
    }
  }
}

// Extracts merge field from an URL
var extractMergedFieldFromUrl = function(htmltag, urlWithMergedField) {
  var regex = /\[\%\(\S*\)\%\]/i;
  if (htmltag === "href" && urlWithMergedField.match(regex)) {
    return urlWithMergedField.match(regex)[0];
  }
  return null;
};
```

il y a tout de même besoin de changer un peu celui-ci afin de lacner la test-suite  
lié a \_getElements appelé sur le this qui est quelque chose propre a telerik
