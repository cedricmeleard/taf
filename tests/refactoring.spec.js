const {
  extractLastPartOfURL,
  isMatchingMergedFieldInUrl,
  isNotContainingAProtocolOrMergedField,
  replaceSrcWithCorrectedUrl,
  replaceHrefWitchCorrectedUrl,
  updateElementsUrl
} = require("../src/refactoring");

describe("Testing Syfadis editor when element is updated", () => {
  it("If an attrname is href and url contain a MergedField should return true", () => {
    let result = isMatchingMergedFieldInUrl(
      "http://localhost/[%(DATASOURCE_OBECT)%]"
    );
    expect(result).toBe(true);
  });

  it("If an attrname is href and url does not contain a MergedField should return false", () => {
    let result = isMatchingMergedFieldInUrl("http://localhost/index.html");
    expect(result).toBe(false);
  });

  it("extract last part of something http://localhost/[%(DATASOURCE_OBECT)%] should [%(DATASOURCE_OBECT)%]", () => {
    let result = extractLastPartOfURL(
      "http://localhost/[%(DATASOURCE_OBECT)%]"
    );
    expect(result).toBe("[%(DATASOURCE_OBECT)%]");
  });

  it("extract last part of something not containing / 'astring' should whole string", () => {
    let result = extractLastPartOfURL("astring");
    expect(result).toBe("astring");
  });

  it("Check if value contains a protocol or merged field when given 'astring' should return true", () => {
    let result = isNotContainingAProtocolOrMergedField("astring");
    expect(result).toBe(true);
  });

  it("Check if value contains a protocol or merged field when given 'http://localhost' should return false", () => {
    let result = isNotContainingAProtocolOrMergedField("http://localhost");
    expect(result).toBe(false);
  });

  it("Check if value contains any protocol, will test all", () => {
    let result = isNotContainingAProtocolOrMergedField("http://localhost");
    expect(result).toBe(false);

    result = isNotContainingAProtocolOrMergedField("https://localhost");
    expect(result).toBe(false);

    result = isNotContainingAProtocolOrMergedField("mailto:test@test.com");
    expect(result).toBe(false);

    result = isNotContainingAProtocolOrMergedField("[%(TEST)%]");
    expect(result).toBe(false);
  });

  it("Check replacement s to %20, should be prefix by window.location", () => {
    let tempDiv = document.createElement("div");
    let result = replaceSrcWithCorrectedUrl(tempDiv, "my image.png");
    expect(result).toBe("http://localhost/my%20image.png");
  });

  it('Check replacement " to %22, should be prefix by window.location', () => {
    let tempDiv = document.createElement("div");
    let result = replaceSrcWithCorrectedUrl(tempDiv, 'my"image.png');
    expect(result).toBe("http://localhost/my%22image.png");
  });

  it('Check no replacement of " to %22, should be prefix by window.location', () => {
    let tempDiv = document.createElement("div");
    let result = replaceSrcWithCorrectedUrl(tempDiv, "myImage.png");
    expect(result).toBe("http://localhost/myImage.png");
  });

  it('Check no replacement of " to %22, should not be prefix by window.location', () => {
    let tempDiv = document.createElement("div");
    let result = replaceSrcWithCorrectedUrl(
      tempDiv,
      "http://google.fr/myImage.png"
    );
    expect(result).toBe("http://google.fr/myImage.png");
  });

  it('Check no replacement of " to %22, should be prefix by window.location', () => {
    let tempDiv = document.createElement("div");
    let result = replaceHrefWitchCorrectedUrl(tempDiv, "/myImage.png");
    expect(result).toBe("http://localhost/myImage.png");
  });
});

describe("Testing editor_UpdateElments whole function", () => {
  let findElments = null;

  beforeEach(() => {
    findElments = function (contentElement, tagName) {
      let result = contentElement.querySelectorAll(tagName);
      return result;
    };
  });

  it("when called with no link inside content, should do nothing", () => {
    let dummy = document.createElement("div");
    updateElementsUrl(dummy, "a", "href", findElments);
    expect(dummy.outerHTML).toBe("<div></div>");
  });

  it("when called with a external link inside content, should not change it", () => {
    let dummy = document.createElement("div");
    let newlink = document.createElement("a");
    newlink.href = "http://google.fr";
    dummy.appendChild(newlink);

    updateElementsUrl(dummy, "a", "href", findElments);

    expect(dummy.outerHTML).toBe('<div><a href="http://google.fr"></a></div>');
  });

  it("when called with a external https link inside content, should not change it", () => {
    let dummy = document.createElement("div");
    let newlink = document.createElement("a");
    newlink.href = "https://google.fr";
    dummy.appendChild(newlink);

    updateElementsUrl(dummy, "a", "href", findElments);

    expect(dummy.outerHTML).toBe('<div><a href="https://google.fr"></a></div>');
  });

  it("when called with a mailto link inside content, should not change it", () => {
    let dummy = document.createElement("div");
    let newlink = document.createElement("a");
    newlink.href = "mailto:test@test.com";
    dummy.appendChild(newlink);

    updateElementsUrl(dummy, "a", "href", findElments);

    expect(dummy.outerHTML).toBe(
      '<div><a href="mailto:test@test.com"></a></div>'
    );
  });

  it("when called with an internal link inside content, should change it with base site", () => {
    let dummy = document.createElement("div");
    let newlink = document.createElement("a");
    newlink.href = "/Users/12345";
    dummy.appendChild(newlink);

    updateElementsUrl(dummy, "a", "href", findElments);

    expect(dummy.outerHTML).toBe(
      '<div><a href="http://localhost/Users/12345"></a></div>'
    );
  });

  it("when called with a merged field inside content, should change it to merged field only", () => {
    let dummy = document.createElement("div");
    let newlink = document.createElement("a");
    newlink.href = "http://localhost/[%(DATASOURCE)%]";
    dummy.appendChild(newlink);

    updateElementsUrl(dummy, "a", "href", findElments);

    expect(dummy.outerHTML).toBe('<div><a href="[%(DATASOURCE)%]"></a></div>');
  });

  it("when called with an image with quote inside content, should change it to url encoded", () => {
    let dummy = document.createElement("div");
    let newimage = document.createElement("img");
    newimage.src = '/my image "with quote".png';
    dummy.appendChild(newimage);

    updateElementsUrl(dummy, "img", "src", findElments);

    expect(dummy.outerHTML).toBe(
      '<div><img src="http://localhost/my%20image%20%22with%20quote%22.png"></div>'
    );
  });
});