const editor_UpdateElementsForTests = require("../src/origin");

describe("Testing editor_UpdateElments whole function", () => {

    let getElements = null;

    beforeEach(() => {
        getElements = function (contentElement, tagName) {
            let result = contentElement.querySelectorAll(tagName);
            return result;
        };
    });


    it("when called with no link inside content, should do nothing", () => {
        let dummy = document.createElement("div");
        editor_UpdateElementsForTests(dummy, "a", "href", getElements);

        expect(dummy.outerHTML).toBe("<div></div>");
    });

    it("when called with a external link inside content, should not change it", () => {
        let dummy = document.createElement("div");
        let newlink = document.createElement("a");
        newlink.href = "http://google.fr";
        dummy.appendChild(newlink);
        editor_UpdateElementsForTests(dummy, "a", "href", getElements);

        expect(dummy.outerHTML).toBe('<div><a href="http://google.fr"></a></div>');
    });

    it("when called with a external https link inside content, should not change it", () => {
        let dummy = document.createElement("div");
        let newlink = document.createElement("a");
        newlink.href = "https://google.fr";
        dummy.appendChild(newlink);

        editor_UpdateElementsForTests(dummy, "a", "href", getElements);

        expect(dummy.outerHTML).toBe('<div><a href="https://google.fr"></a></div>');
    });

    it("when called with a mailto link inside content, should not change it", () => {
        let dummy = document.createElement("div");
        let newlink = document.createElement("a");
        newlink.href = "mailto:test@test.com";
        dummy.appendChild(newlink);

        editor_UpdateElementsForTests(dummy, "a", "href", getElements);

        expect(dummy.outerHTML).toBe(
            '<div><a href="mailto:test@test.com"></a></div>'
        );
    });

    it("when called with an internal link inside content, should change it with base site", () => {
        let dummy = document.createElement("div");
        let newlink = document.createElement("a");
        newlink.href = "/Users/12345";
        dummy.appendChild(newlink);

        editor_UpdateElementsForTests(dummy, "a", "href", getElements);

        expect(dummy.outerHTML).toBe(
            '<div><a href="http://localhost/Users/12345"></a></div>'
        );
    });

    it("when called with a merged field inside content, should change it to merged field only", () => {
        let dummy = document.createElement("div");
        let newlink = document.createElement("a");
        newlink.href = "http://localhost/[%(DATASOURCE)%]";
        dummy.appendChild(newlink);

        editor_UpdateElementsForTests(dummy, "a", "href", getElements);

        expect(dummy.outerHTML).toBe('<div><a href="[%(DATASOURCE)%]"></a></div>');
    });

    it("when called with an image with quote inside content, should change it to url encoded", () => {
        let dummy = document.createElement("div");
        let newimage = document.createElement("img");
        newimage.src = '/my image "with quote".png';
        dummy.appendChild(newimage);

        editor_UpdateElementsForTests(dummy, "img", "src", getElements);

        expect(dummy.outerHTML).toBe(
            '<div><img src="http://localhost/my%20image%20%22with%20quote%22.png"></div>'
        );
    });
});