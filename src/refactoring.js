/**
 * Base function called by telerik to update editor html field
 * should chack and correct url of href/src to correct value
 * and souhld preserve mergedfield
 * @param {Dom Element} contentElement
 * @param {string} tagName
 * @param {string} attrName
 */
function editor_UpdateElements(contentElement, tagName, attrName) {
  //since findElmentsWithTagname call telerik we need to extract this method
  updateElementsUrl(contentElement, tagName, attrName, findElmentsWithTagname);
}

/**
 * Update href and src to correct unescaped or base site invalid values
 * @param {DOM element} contentElement
 * @param {string} tagName
 * @param {string} attrName
 * @param {function(element, tagname)} findElments
 */
var updateElementsUrl = function (
  contentElement,
  tagName,
  attrName,
  findElments
) {
  //try regroup not testable element
  var elements = findElments(contentElement, tagName);
  //TODO maybe this should be replacer's responsability in next refactoring
  var tempDiv = contentElement.ownerDocument.createElement("div");

  if (elements) {
    //TODO this is es6 compatible, should change to ecmascript 3
    for (var element of elements) {
      var attrValue = element.getAttribute(attrName, 2);

      if (attrValue) {
        switch (attrName) {
          case "href":
            if (isNotContainingAProtocolOrMergedField(attrValue)) {
              element.setAttribute(
                "href",
                replaceHrefWitchCorrectedUrl(tempDiv, attrValue)
              );
            } else if (isMatchingMergedFieldInUrl(attrValue)) {
              element.setAttribute("href", extractLastPartOfURL(attrValue));
            }
            break;
          case "src":
            element.setAttribute(
              "src",
              replaceSrcWithCorrectedUrl(tempDiv, attrValue)
            );

            break;
        }
      }
    }
  }
  //TODO why then is set to nulll
  tempDiv.innerHTML = "";
  tempDiv = null;
};

/**
 * Encode src \" with %22 and prefixed by current url
 * @param {DOM element} tempDiv
 * @param {string} attrValue
 */
var replaceSrcWithCorrectedUrl = function (tempDiv, attrValue) {
  tempDiv.innerHTML = '<img src="' + attrValue.replace(/\"/gi, "%22") + '" />';
  return tempDiv.childNodes[0].src;
};

var findElmentsWithTagname = function (contentElement, tagName) {
  return this._getElements(contentElement, tagName);
};

/**
 * Check wether or not value contains a protocol or a mergedfield
 * @param {string} attrValue
 */
var isNotContainingAProtocolOrMergedField = function (attrValue) {
  return (
    attrValue.search("[%(*)%]") == -1 &&
    attrValue.search("http://") == -1 &&
    attrValue.search("https://") == -1 &&
    attrValue.search("mailto:") == -1
  );
};
/**
 * set well formated href attribute to element
 * @param {DOM element} tempDiv
 * @param {string} attrValue
 * @returns {string} new url
 */
var replaceHrefWitchCorrectedUrl = function (tempDiv, attrValue) {
  var protocol = window.location.protocol;
  var host = window.location.host;
  var path = protocol + "//" + host;

  tempDiv.innerHTML = '<a href="' + path + attrValue + '"></a>';
  return tempDiv.childNodes[0].href;
};

/**
 * Check if there is a mergedfield in url
 * @param {string} urlWithMergedField
 */
var isMatchingMergedFieldInUrl = function (urlWithMergedField) {
  return urlWithMergedField.search("[%(*)%]") > 0;
};

/**
 * Extract the end of an url
 * @param {string} urlWithMergedField
 */
var extractLastPartOfURL = function (urlWithMergedField) {
  var arr = urlWithMergedField.split("/");
  return arr[arr.length - 1];
};

const container = {
  extractLastPartOfURL,
  isMatchingMergedFieldInUrl,
  isNotContainingAProtocolOrMergedField,
  replaceHrefWitchCorrectedUrl,
  replaceSrcWithCorrectedUrl,
  updateElementsUrl
};

module.exports = container;