function editor_UpdateElements(contentElement, tagName, attrName) {
    editor_UpdateElementsForTests(contentElement, tagName, attrName, getElements);
}
/**
 * Extract for tests
 */
function editor_UpdateElementsForTests(contentElement, tagName, attrName, getElements) {
    var elements = getElements(contentElement, tagName);

    if (elements) {
        for (var i = 0; i < elements.length; i++) {
            var attrValue = elements[i].getAttribute(attrName, 2);

            var mergeFieldFound = extractMergedFieldFromUrl(attrName, attrValue);
            if (mergeFieldFound !== null) {
                elements[i].setAttribute("href", mergeFieldFound);
            } else {
                var arr = window.location.href.split("/");
                var path = arr[0] + "//" + arr[2];
                if (
                    "href" === attrName &&
                    attrValue &&
                    attrValue.search("[%(*)%]") == -1 &&
                    attrValue.search("http://") == -1 &&
                    attrValue.search("https://") == -1 &&
                    attrValue.search("mailto:") == -1
                ) {
                    elements[i].setAttribute("href", '"' + path + attrValue + '"');
                } else if ("src" == attrName && attrValue) {
                    elements[i].setAttribute(
                        "src",
                        '"' + path + attrValue.replace(/\"/gi, "%22") + '"'
                    );
                }
            }
        }
    }
}

/**
 * Needed for test
 */
var getElements = (contentElement, tagName) => {
    return this._getElements(contentElement, tagName);
}

// Extracts merge field from an URL
var extractMergedFieldFromUrl = function (htmltag, urlWithMergedField) {
    var regex = /\[\%\(\S*\)\%\]/i;
    if (htmltag === "href" && urlWithMergedField.match(regex)) {
        return urlWithMergedField.match(regex)[0];
    }
    return null;
};

module.exports = editor_UpdateElementsForTests;